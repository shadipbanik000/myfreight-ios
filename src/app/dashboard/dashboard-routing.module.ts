import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';
import { HistoryComponent } from './history/history.component';
import { ProfileComponent } from './profile/profile.component';
import { TrakingHistoryComponent } from './traking-history/traking-history.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage,
    children:[
      {path:'', component:ProfileComponent},
      {path:'profile', component:ProfileComponent},
      {path:'history', component:HistoryComponent},
      {path:'',redirectTo:'profile', pathMatch: 'full'}

    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
