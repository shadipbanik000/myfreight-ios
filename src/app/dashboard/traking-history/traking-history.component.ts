import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PreviewAnyFile } from '@awesome-cordova-plugins/preview-any-file/ngx';
import { ModalController, NavParams } from '@ionic/angular';
import { LoaderService } from 'src/app/service/loader.service';
@Component({
  selector: 'app-traking-history',
  templateUrl: './traking-history.component.html',
  styleUrls: ['./traking-history.component.scss'],
})
export class TrakingHistoryComponent implements OnInit {
  constructor(private modalController: ModalController, private httpClient: HttpClient, private navPrams: NavParams,private loading:LoaderService, private previewAnyFile: PreviewAnyFile) { }
  history = this.navPrams.get('data');
  ngOnInit() {
    console.log(this.navPrams.get('data'))
  }
  dissmissModal() {
    this.modalController.dismiss()
  }
  async lablelView(data) {
    this.loading.presentLoading()
    if (data.courierName == "COURIERPLEASE" || data.courierName == "SHERPA" || data.courierName == "HEX" || data.courierName == "ALLIED_EXPRESS") {
      await this.httpClient.get(data.labelUrl, { responseType: 'text' }).subscribe(response => {
        if (response) {
          this.previewAnyFile.previewBase64(response, { mimeType: 'application/pdf' }).subscribe((res: any) => {
           this.loading.dismissLoading();
            console.log(res)
            
          })
        }
      })
    } else {
      this.previewAnyFile.previewPath(data.labelUrl).subscribe((res: any) => {
        this.loading.dismissLoading();
        console.log(res)
      })

    }
  }
}
