import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PreviewAnyFile } from '@awesome-cordova-plugins/preview-any-file/ngx';
import { ApiService } from 'src/app/service/api.service';
import { LoaderService } from 'src/app/service/loader.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit {
  history = [];
  username: string = localStorage.getItem('name');
  constructor(private apiServices: ApiService, private httpClient: HttpClient,private loading:LoaderService ,private previewAnyFile: PreviewAnyFile) { }
  async ngOnInit() {
    await this.getBookingInfo()
  }

  async getBookingInfo() {
    await this.apiServices.api('get', 'booking/details/get-by-username/' + JSON.parse(this.username), {}, true)
      .then((object) => {
        let res1 = object.response;
        console.log(res1)
        if (res1.length > 0) {
          this.history = res1;
        } else {
          return false
        }
      }).catch(err => {
        console.log(err)
      })
  }

  async lablelView(data) {
    this.loading.presentLoading()
    if (data.courierName == "COURIERPLEASE" || data.courierName == "SHERPA" || data.courierName == "HEX" || data.courierName == "ALLIED_EXPRESS") {
      await this.httpClient.get(data.labelUrl, { responseType: 'text' }).subscribe(response => {
        if (response) {
          this.previewAnyFile.previewBase64(response, { mimeType: 'application/pdf' }).subscribe((res: any) => {
            this.loading.dismissLoading();
            console.log(res)

          })
        }
      })
    } else {
      this.previewAnyFile.previewPath(data.labelUrl).subscribe((res: any) => {
        this.loading.dismissLoading();
        console.log(res)
      })

    }
  }
}
