import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { ProfileComponent } from './profile/profile.component';
import { HistoryComponent } from './history/history.component';
import { TrakingHistoryComponent } from './traking-history/traking-history.component';
import { SafePipe } from '../share/pipe/safe.pipe';
import { PreviewAnyFile } from '@awesome-cordova-plugins/preview-any-file/ngx';
import { HomePage } from '../home/home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule
  ],
  declarations: [
    DashboardPage,
    ProfileComponent,
    HistoryComponent,
    TrakingHistoryComponent,
    SafePipe
  ],
  providers:[
    PreviewAnyFile
  ],
  entryComponents:[TrakingHistoryComponent],
  exports:[
    SafePipe
  ]
})
export class DashboardPageModule {}
