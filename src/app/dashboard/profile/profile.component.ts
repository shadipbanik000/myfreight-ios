import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { LoaderService } from 'src/app/service/loader.service';
import { TrakingHistoryComponent } from '../traking-history/traking-history.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  username: string = localStorage.getItem('name');
  user: any;
  awaiting = [];
  inTransit = [];
  onBoard = [];
  delivered = [];
  constructor(private activatedRouter: ActivatedRoute, private platform: Platform, private apiServices: ApiService, private modalController: ModalController, private ladingService: LoaderService, private router: Router) {
    this.getBookingInfo();
  }
  ngOnInit() {
    if (this.activatedRouter.snapshot.queryParams.user) {
      this.user = JSON.parse(this.activatedRouter.snapshot.queryParams.user);
    } else {
      this.getUserDetails();
    }
  }

  getUserDetails() {
    console.log(this.username)
    this.apiServices.api('get', 'user/get-by-username/' + JSON.parse(this.username), {}, true)
      .then((object) => {
        let res1 = object.response;
        this.user = res1;
        console.log(this.user);
        // return res1;
      }).catch(err => {
        console.log(err)
      })
  }

  getBookingInfo() {
    console.log(this.username)
    this.apiServices.api('get', 'booking/details/get-by-username/' + JSON.parse(this.username), {}, false)
      .then((object) => {
        let res1 = object.response;
        console.log(res1)
        if (res1.length > 0) {
          this.getTrackingStatus(res1)
        } else {
          return false
        }
      }).catch(err => {
        console.log(err)
      })
  }
  getTrackingStatus(orders) {
    console.log(orders)
    if (orders.length > 0) {
      for (let i = 0; i < orders.length; i++) {
        this.apiServices.api('get', 'booking/track/' + orders[i].trackingNumber, {}, false)
          .then((object) => {
            let res1 = object.response;
            if (res1 && res1.trackingStatus) {
              switch (res1.trackingStatus) {
                case "IN_TRANSIT":
                  this.inTransit.push(orders[i]);
                  break;
                case "ONBOARD_FOR_DELIVERY":
                  this.onBoard.push(orders[i]);
                  break;
                case "DELIVERED":
                  this.delivered.push(orders[i]);
                case "AWAITING_FOR_PICKUP":
                default:
                  this.awaiting.push(orders[i]);
                  break;
              }
            }
          }).catch(err => {
            console.log(err)
          })
      }
    }
  }

  //  statusView(data){
  //     console.log(data)
  //      let info =JSON.stringify(data,null);

  //  }
  async statusView(data) {
    const modal = await this.modalController.create({
      component: TrakingHistoryComponent,
      cssClass: 'my-custom-class',
      componentProps: { 'data': data },
      mode:'ios',
      backdropDismiss: true,
      swipeToClose: true
    });
    return await modal.present();
  }

}

