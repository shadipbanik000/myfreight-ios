import { Location } from '@angular/common';
import { Component, OnInit,ViewChildren,QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, Platform,IonRouterOutlet } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList < IonRouterOutlet > ;
  constructor(private actionSheetController:ActionSheetController,private route:Router,private platform:Platform, private location:Location) {
     this.backButton()
   }
  title:string="My Profile";
  async ngOnInit() {
    if(!localStorage.getItem('auth_token')){
     await this.route.navigate(['/'])
    }
    if(this.route.url==="/dashboard/profile"){
      this.title="My Profile";
    }else{
      this.title="My History "
    }

  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Action',
      mode:"ios",
      buttons: [
        {
          text: 'LOG OUT',
          role: 'destructive',
          icon: 'exit',
          handler: () => {
            localStorage.clear();
            this.route.navigate(['/'])
          },
        },
        
      ],
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
 titleChange(){
    console.log(this.route.url)
    if(this.route.url==="/dashboard/profile"){
      this.title="My History";
    }else{
      this.title="My Profile "
    }
  }
  backButton() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.routerOutlets.forEach(async(outlet: IonRouterOutlet) => {
          // await this.router.navigate(['/']);
          if(this.route.url.startsWith('/dashboard/')){
            this.location.back(); 
          }
          this.titleChange()
      });
    });
  }
}

