import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toast:ToastController) { }
   isToast:boolean=false
  async presentToast(message,color) {
      this.isToast=true;
       return await this.toast.create({
      message: message,
      color:color,
      mode:'ios',
      position:'bottom',
      duration: 4000,
      buttons: [
        {
          side: 'end',
          icon: 'close',
          role:'cancel',
          handler: () => {
            console.log('close clicked');
          }
        }
      ]
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isToast) {
          this.isToast=false;
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });;
  }



}
