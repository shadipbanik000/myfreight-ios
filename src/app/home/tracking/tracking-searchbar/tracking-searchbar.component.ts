import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { LoaderService } from 'src/app/service/loader.service';
import { ToastService } from 'src/app/service/toast.service';
import { TrackingStatusComponent,modelData } from '../tracking-status/tracking-status.component';

@Component({
  selector: 'app-tracking-searchbar',
  templateUrl: './tracking-searchbar.component.html',
  styleUrls: ['./tracking-searchbar.component.scss'],
})
export class TrackingSearchbarComponent implements OnInit {

  constructor(private modelCtrl:ModalController,private toast:ToastService,private loader:LoaderService, private router:Router, private apiService:ApiService) { }

  ngOnInit() {}
   submitForm(data){
     if(data){
       this.apiService.api('get','booking/track/'+data.orderNumber).then(Object=>{
        let res = JSON.stringify(Object.response,null,) ;
        this.router.navigate(['/trackingStatus'],{queryParams:{res}})
       }).catch(err=>{
         if(err.status==500){
           this.toast.presentToast('Invalid order number','danger')
         }
       })
     }
  }
  toastShow(){
     this.loader.presentLoading()
  }
}
