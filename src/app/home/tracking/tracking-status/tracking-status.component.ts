import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { ToastService } from 'src/app/service/toast.service';
export interface modelData {
  data?: any;
}

@Component({
  selector: 'app-tracking-status',
  templateUrl: './tracking-status.component.html',
  styleUrls: ['./tracking-status.component.scss'],
})
export class TrackingStatusComponent implements OnInit {
  toggleValue:boolean=false
  constructor(private modelCtrl:ModalController,private fcm:FCM, private activateRoute: ActivatedRoute ,private toaster:ToastService) { }
  statusDetails:any= JSON.parse(this.activateRoute.snapshot.queryParams.res);

  ngOnInit() {
     console.log(this.statusDetails)
  }
  notify(trackingId){
    console.log(trackingId)
    this.toggleValue= !this.toggleValue;
    if(this.toggleValue){
        this.fcm.subscribeToTopic(trackingId).then(task=>{
          console.log(task)
          this.toaster.presentToast('Enabled Notification','dark')

        })
    }else{
      this.fcm.unsubscribeFromTopic(trackingId).then(task=>{
          console.log(task)
          this.toaster.presentToast('Disabled Notification','dark')

      })
    }
  }
}
